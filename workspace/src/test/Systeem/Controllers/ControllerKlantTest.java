/**
 * @creator B4
 * @date    4-nov-2014
 * @version 7.1
 */
package test.Systeem.Controllers;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.fail;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

import test.Systeem.Factories;
import Systeem.BusinessDomain.Behandelingtraject;
import Systeem.BusinessDomain.Diagnose;
import Systeem.BusinessDomain.Klant;
import Systeem.BusinessDomain.Verzekering;
import Systeem.Datastorage.DAO.DAOKlant;
import Systeem.Klanten.Businesslogic.ControllerKlant;
import static org.mockito.Mockito.*;
// TODO: Auto-generated Javadoc
/**
 * The Class ControllerKlantTest.
 */
public class ControllerKlantTest {

	/**
	 * Sets the up before class.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@BeforeClass
	public static void setUpBeforeClass() throws Exception {
	}

	/**
	 * Sets the up.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@Before
	public void setUp() throws Exception {
	}

	/**
	 * Tear down.
	 *
	 * @throws Exception
	 *             the exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	/**
	 * Test method for
	 * {@link Systeem.Klanten.Businesslogic.ControllerKlant#geefKlant(java.lang.String)}
	 * .
	 */
	@Test
	public void testGeefKlant() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link Systeem.Klanten.Businesslogic.ControllerKlant#geefKlanten()}.
	 */
	@Test
	public void testGeefKlanten() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link Systeem.Klanten.Businesslogic.ControllerKlant#zoekKlanten(java.lang.String)}
	 * .
	 */
	@Test
	public void testZoekKlanten() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link Systeem.Klanten.Businesslogic.ControllerKlant#klantOpslaan(Systeem.BusinessDomain.Klant)}
	 * .
	 */
	@Test
	public void testKlantOpslaan() {
		fail("Not yet implemented"); // TODO
	}

	/**
	 * Test method for
	 * {@link Systeem.Klanten.Businesslogic.ControllerKlant#geefAlleKlanten()}.
	 */
	@Test
	public void testGeefAlleKlanten() {
		// setup
		List<Behandelingtraject> bAL = new ArrayList<Behandelingtraject>();
		bAL.add(Factories.behandelingTraject("1"));

		List<Verzekering> vAL = new ArrayList<Verzekering>();
		vAL.add(Factories.verzekeringFactory("1", Factories.verzekeringmaatschappijFactory("1"), bAL));

		List<Diagnose> dAL = new ArrayList<Diagnose>();
		dAL.add(Factories.diagnoseFactory("22", bAL));

		List<Klant> kAL = new ArrayList<Klant>();
		kAL.add(Factories.klantFactory("1", vAL, dAL, bAL));
		kAL.add(Factories.klantFactory("2", vAL, dAL, bAL));
		kAL.add(Factories.klantFactory("3", vAL, dAL, bAL));
		ControllerKlant controller = null;

		DAOKlant mockedDAO = mock(DAOKlant.class);

		try {
			when(mockedDAO.geefAlles()).thenReturn(kAL);
			controller = new
					ControllerKlant(); // set dao Field field =
			Field field = ControllerKlant.class.getDeclaredField("daoKlant");
			field.setAccessible(true);
			field.set(controller, mockedDAO);
		}
		catch (Exception e) {
			fail(e.toString());
		}

		List<Klant> result = null;
		List<Klant> expectedResult = kAL;

		try {
			result = controller.geefAlleKlanten();
		}
		catch (IOException e) {
			fail(e.toString());
		}

		assertEquals(expectedResult, result);

	}
}
