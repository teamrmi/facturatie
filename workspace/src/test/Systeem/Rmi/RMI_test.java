/**
 * 
 */
package test.Systeem.Rmi;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertNotNull;

import java.net.MalformedURLException;
import java.rmi.Naming;
import java.rmi.NotBoundException;
import java.rmi.RemoteException;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import Systeem.Settings.RmiSettings;
import Systeem.rmi.business.RemoteKlantenInterface;
import Systeem.rmi.domain.RmiKlantIRead;
import Systeem.rmi.main.RmiServer;

/**
 * @author Gregor
 *
 */
public class RMI_test {

	private static final String HOST = RmiSettings.RmiKlantHost;
	private static final String SERVICE_NAME = RmiSettings.RmiKlantServiceName;

	/**
	 * @throws java.lang.Exception
	 */
	@Before
	public void setUp() throws Exception {

		RmiServer server = new RmiServer();
		server.run();
		Thread.sleep(2000);
	}

	/**
	 * @throws java.lang.Exception
	 */
	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void getKlanten() {
		RemoteKlantenInterface remoteKlantInterface;
		List<RmiKlantIRead> klanten = new ArrayList<>();

		try {
			remoteKlantInterface = (RemoteKlantenInterface) Naming.lookup("//" + HOST + "/" + SERVICE_NAME);
			klanten = remoteKlantInterface.geefAlleKlanten();
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			System.out.println("Url klopt niet");
			e.printStackTrace();
		}

		assertFalse(klanten.isEmpty());
	}

	@Test
	public void geefKlant() {
		RemoteKlantenInterface remoteKlantInterface;
		RmiKlantIRead klant = null;

		try {
			remoteKlantInterface = (RemoteKlantenInterface) Naming.lookup("//" + HOST + "/" + SERVICE_NAME);
			klant = remoteKlantInterface.geefKlant("69854752");
		} catch (RemoteException | NotBoundException e) {
			e.printStackTrace();
		} catch (MalformedURLException e) {
			System.out.println("Url klopt niet");
			e.printStackTrace();
		}

		assertNotNull(klant);
	}

}
